﻿#include <iostream>
#include <algorithm> 

using namespace std;

class Player {
public:
    string name;
    int points;
};

bool comparePoints(Player p1, Player p2) {
    return p1.points > p2.points;
}

int main() {
    cout << "How many players do you want to add? ";
    int n;
    cin>>n;
    Player* players = new Player[n];
    for (int i = 0; i < n; i++) {
        cout << "Name of the player #" << i + 1 << ": ";
        cin >> players[i].name;
        cout << "Points of the player #" << i + 1 << ": ";
        cin >> players[i].points;
    }
    sort(players, players + n, comparePoints);
    for (int i = 0; i < n; i++)
        cout << "Player " << players[i].name << ": " << players[i].points << endl;
    
    return 0;
}